package main

import (
	"flag"

	"log"

	"github.com/guus-vanweelden/mongoDbExporter/exporter"
	"gopkg.in/mgo.v2"
)

var configFilePath = flag.String("config", "config.json", "path to the config file")
var Cfg *exporter.Config

func main() {
	flag.Parse()
	Cfg := exporter.LoadConfig(*configFilePath)
	session, err := mgo.Dial(Cfg.Server)
	if err != nil {
		log.Panicf("Error by dialing the mongoDb: %s", err)
	}
	defer session.Close()

	c := session.DB(Cfg.Database).C(Cfg.Collection)
	articles := c.Find(nil).Iter()
	article := new(exporter.Article)
	for articles.Next(&article) {
		//log.Printf("Article: %+v", article)
		article.WriteFile(Cfg.Output)
	}
}
