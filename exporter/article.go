package exporter

import (
	"crypto/sha1"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
)

type Article struct {
	Title     string
	Link      string
	Text      string
	Timestamp string
}

type Articles []Article

func simpleGenerateHash(str string, length int) string {
	h := sha1.New()
	io.WriteString(h, strings.ToLower(str))
	hash := fmt.Sprintf("%x", h.Sum(nil))[0:length]
	return hash
}

func GenerateHash(str string) string {
	return simpleGenerateHash(str, 12)
}

func (article *Article) WriteFile(output string) {
	_ = ioutil.WriteFile(output+GenerateHash(article.Title)+".txt", []byte(article.Text), 0644)
}
