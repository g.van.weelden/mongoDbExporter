package exporter

import (
	"encoding/json"

	"io/ioutil"
	"log"
)

type Config struct {
	Server     string
	Database   string
	Collection string
	Output     string
}

func LoadConfig(filename string) *Config {
	configFile, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Panicf("Error by reading config file: %s", err)
	}
	cfg := new(Config)
	err = json.Unmarshal(configFile, cfg)
	if err != nil {
		log.Panicf("Error by unmarshalling config file: %s", err)
	}

	return cfg
}
